<!DOCTYPE HTML>
<html lang="es">
<head>
<meta charset="UTF-8" />
<style>
.error {color: #FF0000;}
</style>
</head>
<body>

<?php
$es = array("E.S.O.", "Batxillerat", "SMX", "DAW", "DAM", "ASIX", "Altres" );
// define variables and set to empty values
$nameErr = $emailErr = $estudisErr = $websiteErr = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $_SESSION["name"]=test_input($_POST["name"]);
  $_SESSION["email"]=test_input($_POST["email"]);
  $_SESSION["website"]=test_input($_POST["website"]);
  $_SESSION["comment"]=test_input($_POST["comment"]);
  $_SESSION["estudis"]=test_input($_POST["estudis"]);

  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  }
  else if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  }
  else if (empty($_POST["estudis"])) {
    $estudisErr = "Studies is required";
  }
  else {
    $datos=array($_SESSION["name"], $_SESSION["email"], $_SESSION["website"],
                 $_SESSION["comment"], $_SESSION["estudis"]);
    $_SESSION["name"]="";
    $_SESSION["email"]="";
    $_SESSION["website"]="";
    $_SESSION["comment"]="";
    $_SESSION["estudis"]="";
    //Elimino variable de Session
    /*unset($_SESSION['nom']);
      unset($_SESSION['email']);
      unset($_SESSION['website']);
      unset($_SESSION['comment']);
      unset($_SESSION['estudis']);*/
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

<h2>PHP Form Validation Example</h2>
<p><span class="error">* required field.</span></p>
<form name="formu" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
  Name: <input type="text" name="name" value="<?php echo $_SESSION["name"];?>">
  <span class="error">* <?php echo $nameErr;?></span>
  <br><br>
  E-mail: <input type="text" name="email" value="<?php echo $_SESSION["email"];?>">
  <span class="error">* <?php echo $emailErr;?></span>
  <br><br>
  Website: <input type="text" name="website" value="<?php echo $_SESSION["website"];?>">
  <span class="error"><?php echo $websiteErr;?></span>
  <br><br>
  Comment: <textarea name="comment" rows="5" cols="40"><?php echo $_SESSION["comment"];?></textarea>
  <br><br>
  Studies:
  <select name="estudis">
    <option value=""><< Selecciona una opci� >></option>
  <?php
      foreach($es as $row){
        if($row==$_SESSION["estudis"]){
  ?>
    <option value="<?php echo $row;?>" selected><?php echo $row;?></option>
  <?php
        }
        else{
  ?>
    <option value="<?php echo $row;?>"><?php echo $row;?></option>
  <?php
        }
      }
  ?>
  </select>
  <span class="error">* <?php echo $estudisErr;?></span>
  <br><br>
  <input type="submit" name="submit" value="Submit">
</form>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
if (empty($_POST["name"])) {
?>
<script type="text/javascript">
  document.formu.name.focus();
</script>
<?php
}
else if (empty($_POST["email"])) {
?>
<script type="text/javascript">
  document.formu.email.focus();
</script>
<?php
}
else if (empty($_POST["estudis"])) {
?>
<script type="text/javascript">
  document.formu.estudis.focus();
</script>
<?php
}
else {
?>
<script type="text/javascript">
  document.formu.name.focus();
</script>
<?php
}
}
?>
<h2>Your Input:</h2>
<?php
  foreach ($datos as $dato) {
    if($dato!=""){
      echo $dato;
?>
<br>
<?php
    }
  }
?>
</body>
</html>
